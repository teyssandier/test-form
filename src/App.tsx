import { FormikForm } from "./components/FormikForm/FormikForm"
import { Form } from "./components/VanillaForm/Form"

function App() { 
    return <>
        <FormikForm/>
        <Form />
    </>
}

export default App

