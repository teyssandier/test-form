import { TextField as MuiTextField } from "@mui/material"
import { styled } from "@mui/material/styles"

export const TextField = styled(MuiTextField)({
    margin: "10px 0",
})

export const FormWrapper = styled("form")({
    display: "flex",
    flexDirection: "column",
    width: "250px",
    margin: "3rem auto",
})
