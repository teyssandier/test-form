import { useFormik } from "formik"
import { Button } from "@mui/material"
import * as Styled from "./FormikForm.styled"
import { FormControlStyled } from "../VanillaForm/input"
import { validationSchema } from "./validationSchema"

export const FormikForm = () => {
    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2))
        },
    })

    return (
        <Styled.FormWrapper onSubmit={formik.handleSubmit}>
            <h2>Formik Form with Lib</h2>

            <FormControlStyled>
                <Styled.TextField
                    id="email"
                    name="email"
                    label="Email"
                    required
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    variant="filled"
                    helperText={formik.touched.email && formik.errors.email}
                    aria-describedby="email"
                />
            </FormControlStyled>
            <FormControlStyled>
                <Styled.TextField
                    id="password"
                    name="password"
                    label="Password"
                    required
                    type="password"
                    variant="filled"
                    aria-describedby="password"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    error={
                        formik.touched.password &&
                        Boolean(formik.errors.password)
                    }
                    helperText={
                        formik.touched.password && formik.errors.password
                    }
                />
            </FormControlStyled>
            <Button color="success" variant="contained" fullWidth type="submit">
                Valider
            </Button>
        </Styled.FormWrapper>
    )
}
