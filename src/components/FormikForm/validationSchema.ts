import * as yup from "yup"

export const validationSchema = yup.object({
    email: yup
        .string()
        .email("Entrez un mail valide")
        .required("Un email est requis"),
    password: yup
        .string()
        .min(8, "Le mot de passe doit contenir au moins 8 caractères")
        .required("Le mot de passe est requis"),
})
