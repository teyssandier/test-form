import { styled } from "@mui/material/styles"

export const FromWrapper = styled("form")({
    display: "flex",
    flexDirection: "column",
    width: "250px",
    margin: "3rem auto",
})
