import { useState, ChangeEvent, FormEvent } from "react"
import {
    validateEmail,
    validatePassword,
    validation,
} from "../../utils/formUtils"
import * as Styled from "./Form.styled"
import { Input } from "./input"
import { Button } from "@mui/material"

export const Form = () => {
    const [formValues, setFormValues] = useState(defaultState)
    const [formErrors, setFormErrors] = useState(defaultErrors)

    const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
        const { id, value } = event.target
        setFormValues((prevState) => ({
            ...prevState,
            [id]: value,
        }))
        setFormErrors((prev) => ({
            ...prev,
            [id]: !validation[id](value),
        }))
    }

    const handleErrors = (): object => {
        const errors = {
            email: !validateEmail(formValues.email),
            password: !validatePassword(formValues.password),
        }
        setFormErrors(errors)
        return errors
    }

    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const errors = handleErrors()
        if (Object.values(errors).every((value) => value === false)) {
            console.log(formValues)
            alert(JSON.stringify(formValues, null, 2))
        }
    }

    return (
        <Styled.FromWrapper onSubmit={onSubmit}>
            <h2>Vanilla Form</h2>

            <Input
                error={formErrors.email}
                handleChange={handleChange}
                id="email"
                value={formValues?.email || ""}
                label="Email"
                errorLabel="L'email n'est pas valide"
                required
                aria-describedby="email"
            />
            <Input
                error={formErrors.password}
                handleChange={handleChange}
                id="password"
                value={formValues?.password || ""}
                label="Mot de passe"
                errorLabel="Le mot de passe doit contenir au moins 8 caractères"
            />
            <Button variant="contained" color="success" type="submit">
                Valider
            </Button>
        </Styled.FromWrapper>
    )
}

const defaultState = {
    email: "",
    password: "",
}

const defaultErrors = {
    email: false,
    password: false,
}
