import {
    FormControl,
    InputProps as MuiInputProps,
    TextField as MuiTextField,
} from "@mui/material"
import { styled } from "@mui/material/styles"
import { ChangeEvent } from "react"

type InputProps = MuiInputProps & {
    value: string
    id: string
    handleChange: (event: ChangeEvent<HTMLInputElement>) => void
    label: string
    error: boolean
    errorLabel: string
}

export const Input = (props: InputProps) => {
    const { handleChange, id, value, label, error, errorLabel } = props

    return (
        <FormControlStyled>
            <MuiTextField
                id={id}
                required
                value={value}
                InputLabelProps={{
                    shrink: true,
                }}
                label={label}
                error={error}
                onChange={handleChange}
                helperText={error && errorLabel}
                aria-describedby="email"
                variant="filled"
            />
        </FormControlStyled>
    )
}

export const FormControlStyled = styled(FormControl)({
    margin: "10px 0",
})
