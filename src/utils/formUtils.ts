type Validate =  (email: string) => boolean

export const validateEmail: Validate = (email: string): boolean => {
    return !!email
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
}


export const validatePassword: Validate = (password: string): boolean => {
    return password.length > 8
}

type Validation = {
    [key: string]: Validate
}

export const validation: Validation = {
    email: validateEmail,
    password: validatePassword,
}